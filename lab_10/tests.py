from django.test import TestCase, Client
from .omdb_api import *
from .utils import *

import os
import environ
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab10UnitTest(TestCase):
  def test_lab_10_custom_auth_login(self):
    response_post = Client().post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    self.assertRedirects(response_post,'/lab-10/', 302, 302)

  def test_lab_10_custom_auth_login_invalid(self):
    response_post = Client().post(
        '/lab-10/custom_auth/login/',
        {'username': 'utest', 'password': 'ptest'},
        HTTP_HOST='localhost:8000'
    )
    self.assertRedirects(response_post,'/lab-10/', 302, 200)

  def test_lab_10_custom_auth_logout(self):
    response = Client().get('/lab-10/custom_auth/logout/')
    self.assertRedirects(response, '/lab-10/', 302, 200)

  def test_lab_10_omdbapi_search_movie(self):
    response = search_movie("Titanic","2012")
    self.assertIsNotNone(response)

  def test_lab_10_omdbapi_get_detail_movie(self):
    response = get_detail_movie("tt1869152")
    self.assertIsNotNone(response)

  def test_lab_10_dashboard_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-10/dashboard/')
    self.assertTemplateUsed(response, 'lab_10/dashboard.html')

  def test_lab_10_dashboard_not_logged_in(self):
    response = Client().get('/lab-10/dashboard/')
    self.assertRedirects(response, '/lab-10/', 302)

  def test_lab_10_movie_list(self):
    Client().get('/lab-10/movie/list/?judul=titanic&tahun=2012')
    response = Client().get('/lab-10/movie/list/')
    self.assertIsNotNone(response)

  def test_lab_10_movie_detail_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    client.get('/lab-10/dashboard/')
    client.get('/lab-10/movie/watch_later/add/tt1869152/')
    client.get('/lab-10/movie/watched/add/tt1869152/')
    response = client.get('/lab-10/movie/detail/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_movie_detail_not_logged_in(self):
    client = Client()
    client.get('/lab-10/movie/watched/add/tt1869152/')
    response = client.get('/lab-10/movie/detail/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_add_watch_later_logged_in(self):
    client = Client()
    client.get('/lab-10/movie/watch_later/add/tt0117050/')
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    client.get('/lab-10/dashboard/')
    response = client.get('/lab-10/movie/watch_later/add/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_add_watch_later_not_logged_in(self):
    client = Client()
    client.get('/lab-10/movie/watch_later/add/tt0117050/')
    response = client.get('/lab-10/movie/watch_later/add/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_list_watch_later_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    client.get('/lab-10/dashboard/')
    client.get('/lab-10/movie/watch_later/add/tt0117050/')
    response = client.get('/lab-10/movie/watch_later/')
    self.assertIsNotNone(response)

  def test_lab_10_list_watch_later(self):
    client = Client()
    client.get('/lab-10/movie/watch_later/add/tt0117050/')
    response = client.get('/lab-10/movie/watch_later/')
    self.assertIsNotNone(response)

  def test_lab_10_add_watched_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    client.get('/lab-10/dashboard/')
    client.get('/lab-10/movie/watch_later/add/tt0117050/')
    client.get('/lab-10/movie/watch_later/add/tt1869152/')
    response = client.get('/lab-10/movie/watched/add/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_add_watched_not_logged_in(self):
    client = Client()
    client.get('/lab-10/movie/watch_later/add/tt1869152/')
    client.get('/lab-10/movie/watched/add/tt0117050/')
    response = client.get('/lab-10/movie/watched/add/tt1869152/')
    self.assertIsNotNone(response)

  def test_lab_10_list_watched_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-10/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    client.get('/lab-10/dashboard/')
    client.get('/lab-10/movie/watched/add/tt0117050/')
    response = client.get('/lab-10/movie/watched/')
    self.assertIsNotNone(response)

  def test_lab_10_list_watched(self):
    client = Client()
    client.get('/lab-10/movie/watched/add/tt0117050/')
    response = client.get('/lab-10/movie/watched/')
    self.assertIsNotNone(response)

  def test_lab_10_api_search_movie(self):
    response = Client().get('/lab-10/api/movie/titanic/2012/')
    self.assertIsNotNone(response)

  def test_lab_10_api_search_movie_empty(self):
    response = Client().get('/lab-10/api/movie/-/-/')
    self.assertIsNotNone(response)
