function showChat() {
  var chatLog = window.localStorage.getItem('chatLog') || '[]'
  chatLog = JSON.parse(chatLog);
  var htmlChatLog = chatLog.map(function(item) {
    return '<div class="chat-msg">'+item+'</div>';
  });
  $('.msg-insert').html(htmlChatLog.join(''));
}

function appendChat(message) {
  var chatLog = window.localStorage.getItem('chatLog') || '[]';
  chatLog = JSON.parse(chatLog);
  chatLog.push(message);
  window.localStorage.setItem('chatLog', JSON.stringify(chatLog));
}

function keydownEnter(e) {
  if (e.which == 13) {
    e.preventDefault();
    appendChat($('#chat-input').val());
    $('#chat-input').val('');
    showChat();
  }
}

function toggleChat() {
  $('.chat-body').toggle();
}

var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

function initThemes() {
  var initial = '['+
    '{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},'+
    '{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},'+
    '{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},'+
    '{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},'+
    '{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},'+
    '{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},'+
    '{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},'+
    '{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},'+
    '{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},'+
    '{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},'+
    '{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}'+
  ']';
  window.localStorage.setItem('themes', initial);
  if(window.localStorage.getItem('selectedTheme') == null) applyTheme(3);
  else {
    applyTheme(JSON.parse(window.localStorage.getItem('selectedTheme')).id);
  }
}

function applyTheme(id) {
  var themes = JSON.parse(window.localStorage.getItem('themes'));
  var theme = themes.filter(function(item) { return item.id == id; })[0];
  window.localStorage.setItem('selectedTheme', JSON.stringify(theme));
  $('body').css('background-color', theme.bcgColor);
  $('#calc-buttons').css('color', theme.fontColor);
}

$( document ).ready(function() {
  showChat();
  initThemes();
  $('#chat-input').keydown(keydownEnter);
  $('#chat-close-button').click(toggleChat);
  $('.my-select').select2();
  $('.my-select').select2({
    'data': JSON.parse(window.localStorage.getItem('themes'))
  });
  $('.apply-button').click(function(){
    applyTheme($('.my-select').val());
  });
});
