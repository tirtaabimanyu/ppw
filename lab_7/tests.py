from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper


class Lab7UnitTest(TestCase):
    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab_7_index_func(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Teman", response.content.decode('utf8'))

    def test_get_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(
            auth_param['client_id'],
            csui_helper.instance.get_auth_param_dict()['client_id']
        )

    def test_get_mahasiswa_list_json(self):
        csui_helper = CSUIhelper()
        mahasiswa_list_json = csui_helper.instance.get_mahasiswa_list(1)
        self.assertIsNotNone(mahasiswa_list_json)

    def test_invalid_sso(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.username = "admin"
        csui_helper.instance.password = "admin"
        with self.assertRaisesMessage(Exception, 'admin'):
            csui_helper.instance.get_access_token()

    def test_as_dict_friend(self):
        friendName = "admin"
        friendNpm = "1"
        Friend.objects.create(friend_name=friendName, npm=friendNpm)
        friend = Friend.objects.get(npm=friendNpm)
        self.assertEqual(friendName, friend.friend_name)
        self.assertEqual(friendNpm, friend.npm)

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "admin", 'npm': "1"},
            HTTP_HOST='localhost:8000'
        )
        self.assertEqual(response_post.status_code, 200)

    def test_delete_friend(self):
        friend = Friend.objects.create(
            friend_name="admin",
            npm="1"
        )
        response = Client().post('/lab-7/delete-friend/' + friend.npm + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/', data={'npm':'1'})
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken': False})
