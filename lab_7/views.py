from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.urls import reverse

from lab_1.views import mhs_name
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json
import requests

response = {}
response['author'] = mhs_name
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    page = request.GET.get('page', '1')
    prev = int(page) - 1
    next = int(page) + 1
    if(prev == 0):
        prev = 1
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list(page)

    friend_list = Friend.objects.all()
    response["mahasiswa_list"] = mahasiswa_list
    response["friend_list"] = friend_list
    response["prev"] = prev
    response["next"] = next
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def get_friend_list(request):
    friend_list = Friend.objects.all()
    return JsonResponse(serializers.serialize('json',friend_list), safe=False)

def friend_list(request):
    html = 'lab_7/daftar_teman.html'
    return render(request, html)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        is_taken = requests.post(request.build_absolute_uri(reverse('lab-7:validate-npm')), data={'npm':npm}).json()['is_taken']
        if(not is_taken):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    is_taken = Friend.objects.filter(npm=npm).exists();
    data = {
        'is_taken': is_taken
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
