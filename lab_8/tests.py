from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Lab8UnitTest(TestCase):
  def test_lab_8_using_index_func(self):
        found = resolve('/lab-8/')
        self.assertEqual(found.func, index)

  def test_lab_8_index_func(self):
      response = Client().get('/lab-8/')
      self.assertEqual(response.status_code, 200)
