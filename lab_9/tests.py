from django.test import TestCase, Client
from http.cookies import SimpleCookie

from .api_enterkomputer import get_drones
from .csui_helper import get_access_token, get_client_id, verify_user
from .custom_auth import auth_login, auth_logout

import os
import environ
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9UnitTest(TestCase):
  def test_lab_9_enterkomputer_get_drones(self):
    drones = get_drones()
    self.assertIsNotNone(drones)

  def test_lab_9_csui_helper_get_access_token(self):
    SSO_USERNAME = env('SSO_USERNAME')
    SSO_PASSWORD = env('SSO_PASSWORD')
    access_token = get_access_token(SSO_USERNAME, SSO_PASSWORD)
    self.assertIsNotNone(access_token)

  def test_lab_9_csui_helper_get_access_token_invalid(self):
    SSO_USERNAME = 'utest'
    SSO_PASSWORD = 'ptest'
    access_token = get_access_token(SSO_USERNAME, SSO_PASSWORD)
    self.assertIsNone(access_token)

  def test_lab_9_csui_helper_get_client_id(self):
    client_id = get_client_id
    self.assertIsNotNone(client_id)

  def test_lab_9_csui_helper_verify_user(self):
    SSO_USERNAME = env('SSO_USERNAME')
    SSO_PASSWORD = env('SSO_PASSWORD')
    access_token = get_access_token(SSO_USERNAME, SSO_PASSWORD)
    response = verify_user(access_token)
    self.assertIsNotNone(response)

  def test_lab_9_custom_auth_login(self):
    response_post = Client().post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    self.assertRedirects(response_post,'/lab-9/', 302, 302)

  def test_lab_9_custom_auth_login_invalid(self):
    response_post = Client().post(
        '/lab-9/custom_auth/login/',
        {'username': 'utest', 'password': 'ptest'},
        HTTP_HOST='localhost:8000'
    )
    self.assertRedirects(response_post,'/lab-9/', 302, 200)

  def test_lab_9_custom_auth_logout(self):
    response = Client().get('/lab-9/custom_auth/logout/')
    self.assertRedirects(response, '/lab-9/', 302, 200)

  def test_lab_9_profile_not_logged_in(self):
    response = Client().get('/lab-9/profile/')
    self.assertRedirects(response, '/lab-9/', 302)

  def test_lab_9_profile_logged_in(self):
    client = Client()
    response_post = client.post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-9/profile/')
    self.assertTemplateUsed(response, 'lab_9/session/profile.html')

  def test_lab_9_profile_with_saved_drones(self):
    client = Client()
    response_post = client.post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-9/add_session_drones/1/')
    response = client.get('/lab-9/profile/')
    self.assertTemplateUsed(response, 'lab_9/session/profile.html')

  def test_lab_9_add_new_fav_drones(self):
    client = Client()
    response_post = client.post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-9/add_session_drones/1/')
    response = client.get('/lab-9/add_session_drones/2/')
    self.assertRedirects(response, '/lab-9/profile/')

  def test_lab_9_delete_fav_drones(self):
    client = Client()
    response_post = client.post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-9/add_session_drones/1/')
    response = client.get('/lab-9/del_session_drones/1/')
    self.assertRedirects(response, '/lab-9/profile/')

  def test_lab_9_clear_fav_drones(self):
    client = Client()
    response_post = client.post(
        '/lab-9/custom_auth/login/',
        {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')},
        HTTP_HOST='localhost:8000'
    )
    response = client.get('/lab-9/add_session_drones/1/')
    response = client.get('/lab-9/clear_session_drones/')
    self.assertRedirects(response, '/lab-9/profile/')

  def test_lab_9_cookie_auth_login(self):
    client = Client()
    response_post = client.post('/lab-9/cookie/auth_login/', data={'username':'utest', 'password':'ptest'})
    self.assertRedirects(response_post, '/lab-9/cookie/login/', 302, 302)

  def test_lab_9_cookie_auth_login_invalid(self):
    client = Client()
    response_post = client.post('/lab-9/cookie/auth_login/', data={'username':'', 'password':''})
    self.assertRedirects(response_post, '/lab-9/cookie/login/')

  def test_lab_9_cookie_auth_login_get(self):
    client = Client()
    response = client.get('/lab-9/cookie/auth_login/')
    self.assertRedirects(response, '/lab-9/cookie/login/')

  def test_lab_9_cookie_login_not_logged_in(self):
    client = Client()
    response = client.get('/lab-9/cookie/login/')
    self.assertTemplateUsed(response, 'lab_9/cookie/login.html')

  def test_lab_9_cookie_profile_not_logged_in(self):
    response = Client().get('/lab-9/cookie/profile/')
    self.assertRedirects(response, '/lab-9/cookie/login/')

  def test_lab_9_cookie_profile(self):
    client = Client()
    client.cookies = SimpleCookie({'user_login':'utest', 'user_password':'ptest'})
    response = client.get('/lab-9/cookie/profile/')
    self.assertTemplateUsed(response, 'lab_9/cookie/profile.html')

  def test_lab_9_cookie_profile_invalid(self):
    client = Client()
    client.cookies = SimpleCookie({'user_login':'', 'user_password':''})
    response = client.get('/lab-9/cookie/profile/')
    self.assertTemplateUsed(response, 'lab_9/cookie/login.html')

  def test_lab_9_cookie_clear(self):
    response = Client().get('/lab-9/cookie/clear/')
    self.assertRedirects(response, '/lab-9/cookie/login/')
